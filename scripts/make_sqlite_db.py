# -*- coding: utf-8 -*-

import sqlite3
import argparse
import os

SQL_CREATE_PEOPLE_TABLE = """ CREATE TABLE IF NOT EXISTS people (
                                        id integer PRIMARY KEY,
                                        name VARCHAR(42) NOT NULL,
                                        picture blob NOT NULL
                                    ); """

PROGRAM_DESCRIPTION = """ This program creates an sqlite3 db from '.jpg' files in a
directory, it creates a table called "people" with the columns: 'id', 'name', 'picture' """


def dir(path):
    if os.path.isdir(path):
        return path
    raise ValueError("not a directory")


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)
 
    return None

def create_table(conn):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(SQL_CREATE_PEOPLE_TABLE)
    except Exception, ex:
        print(e)


def get_image_list(dir):
    return [os.path.join(unicode(dir),i) for i in os.listdir(unicode(dir)) if os.path.splitext(unicode(i))[1] == '.jpg']


def add_person_to_db(conn, img_path):
    with open(unicode(img_path), "rb") as r:
        pic = r.read()
    name = os.path.basename(os.path.splitext(img_path)[0])
    try:
        c = conn.cursor()
        c.execute("INSERT INTO PEOPLE VALUES("
            "NULL, :name, :picture)", dict(name=name, picture=sqlite3.Binary(pic)))
        return c.lastrowid
    except Exception, ex:
        print(ex)



def main():
    parser = argparse.ArgumentParser(description=PROGRAM_DESCRIPTION)
    parser.add_argument(
        '-o', '--output', help="output path for the db, default is 'pictures.db'", type=str, default="pictures.db")
    parser.add_argument('-d', '--directory',
                        help="a directory to collect '.jpg' files from", type=dir, required=True)
    args = parser.parse_args()
    conn = create_connection(args.output)
    create_table(conn)
    for i in get_image_list(args.directory):
        add_person_to_db(conn, i)
    conn.commit()

if __name__ == '__main__':
    main()
