package mai.face.rand;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;

import java.util.ArrayList;
import java.util.List;


public class PersonRepo {
    private DatabaseHelper dbHelper;

    public PersonRepo(Context context) {
        this.dbHelper = new DatabaseHelper(context);
    }

    public List<Person> getRandomPeople(int num_of_people){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery =  "SELECT " +
                Person.KEY_NAME + "," +
                Person.KEY_PICTURE +
                " FROM " + Person.TABLE_NAME
                + " ORDER BY RANDOM() LIMIT " + Integer.toString(num_of_people);

        ArrayList<Person> p = new ArrayList<>();

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                byte[] b = cursor.getBlob(cursor.getColumnIndex(Person.KEY_PICTURE));
                p.add(new Person(cursor.getString(cursor.getColumnIndex(Person.KEY_NAME)),
                        BitmapFactory.decodeByteArray(b, 0, b.length)));
            }while(cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return p;
    }
}

