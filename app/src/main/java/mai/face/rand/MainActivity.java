package mai.face.rand;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    Button buttons[] = new Button[4];
    ImageView chosen_button;
    TextView score_view;
    Person people[] = new Person[4];
    int chosen_person_index;
    Random rand = new Random();
    PersonRepo repo;
    int score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        repo = new PersonRepo(getBaseContext());

        buttons[0] = (Button)findViewById(R.id.button_name1);
        buttons[1] = (Button)findViewById(R.id.button_name2);
        buttons[2] = (Button)findViewById(R.id.button_name3);
        buttons[3] =(Button)findViewById(R.id.button_name4);
        chosen_button = (ImageView)findViewById(R.id.person_image);
        score_view = (TextView)findViewById(R.id.score);

        generateNewQuestion();
    }

    public void generateNewQuestion()
    {
        ArrayList<Person> p = (ArrayList<Person>) repo.getRandomPeople(4);
        chosen_person_index = rand.nextInt(4);
        for (int i = 0; i < 4; ++i) {
            buttons[i].setText(p.get(i).getName());
            if (chosen_person_index == i) {
                buttons[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Button b = (Button) view;
                        score += 2;
                        generateNewQuestion();
                    }
                });
            }
            else {
                buttons[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Button b = (Button) view;
                        score-=1;
                        generateNewQuestion();
                    }
                });
            }

        }

        chosen_button.setImageBitmap(p.get(chosen_person_index).getImage());
        score_view.setText(String.format("SCORE: %s", Integer.toString(score)));
    }
}
