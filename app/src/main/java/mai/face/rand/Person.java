package mai.face.rand;

import android.graphics.Bitmap;

public class Person {
    public static final String TABLE_NAME = "people";
    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_PICTURE = "picture";
    private String name;
    private Bitmap image;

   public Person(String name, Bitmap image)
   {
       this.name = name;
       this.image = image;
   }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
